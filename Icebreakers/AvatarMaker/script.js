$( document ).ready( function() {
  
  /* Item data */
  backgrounds = [ "#ff9393", "#f993ff", "#9c93ff", "#93e1ff", "#93ffc3", "#f9ff93", "#ffc493", "#ffffff" ];
  bases = [ "bases-00.png", "bases-01.png", "bases-02.png", "bases-03.png", "bases-04.png", "bases-05.png", "bases-06.png", "bases-07.png", "bases-08.png", "bases-09.png", "bases-10.png", "bases-11.png", "bases-12.png", "bases-13.png" ];
  outfits = [ "outfits-00.png", "outfits-01.png", "outfits-02.png", "outfits-03.png", "outfits-04.png", "outfits-05.png", "outfits-06.png", "outfits-07.png", "outfits-08.png", "outfits-09.png", "outfits-10.png", "outfits-11.png", "outfits-12.png", "outfits-13.png", "outfits-14.png", "outfits-15.png", "outfits-16.png", "outfits-17.png" ];
  ears = [ "ears-00.png", "ears-01.png", "ears-02.png", "ears-03.png", "ears-04.png", "ears-05.png", "ears-06.png", "ears-07.png", "ears-08.png", "ears-09.png", "ears-10.png", "ears-11.png", "ears-12.png", "ears-13.png", "ears-14.png", "ears-15.png", "ears-16.png" ];
  faces = [ "faces-00.png", "faces-01.png", "faces-02.png", "faces-03.png", "faces-04.png", "faces-05.png", "faces-06.png", "faces-07.png", "faces-08.png", "faces-09.png", "faces-10.png", "faces-11.png", "faces-12.png", "faces-13.png", "faces-14.png" ];
  accessories = [ "accessories-00.png", "accessories-01.png", "accessories-02.png", "accessories-03.png", "accessories-04.png" ];
  hair = [ "hair-00.png", "hair-01.png", "hair-02.png", "hair-03.png", "hair-04.png", "hair-05.png", "hair-06.png", "hair-07.png", "hair-08.png", "hair-09.png", "hair-10.png", "hair-11.png", "hair-12.png", "hair-13.png", "hair-14.png" ];
  
  assetPath = "images/parts/";
  
  /* Events */
  $( ".option-base"       ).click( function() { 
    console.log( ".option-base clicked!" );
    SetBase       ( $( this ).css( "background-image" ) ); 
  } );
  $( ".option-outfit"     ).click( function() { SetOutfit     ( $( this ).css( "background-image" ) ); } );
  $( ".option-ear"        ).click( function() { SetEars       ( $( this ).css( "background-image" ) ); } );
  $( ".option-face"       ).click( function() { SetFace       ( $( this ).css( "background-image" ) ); } );
  $( ".option-accessory"  ).click( function() { SetAccessory  ( $( this ).css( "background-image" ) ); } );
  $( ".option-hair"       ).click( function() { SetHair       ( $( this ).css( "background-image" ) ); } );
  
  $( "button.option-bg" ).click( function() { SetBackground( $( this ).css( "background-color" ) ); } );
  
  // Nav sub-menus
  $( ".option-nav-button" ).click( function() {
    $( ".option-nav-button" ).css( "background-color", "#ffffff" );
    $( this ).css( "background-color", "#ffe36d" );
    
    var goto = $( this ).attr( "id" ).replace( "goto-", "" );
    OpenPane( goto );
  } );
  
  function OpenPane( paneName )
  {
    console.log( "OpenPane(", paneName, ")" );
    var pane = "#" + paneName + "-options";
    
    $( ".options-pane" ).css( "display", "none" );
    $( pane ).css( "display", "block" );
  }
  
  function SetBackground( id )
  {
    console.log( "SetBackground(", id, ")" );
    $( ".viewer" ).css( "background-color", id );
  }
  
  function SetBase( id )
  {
    console.log( "SetBase(", id, ")" );
    $( "#avatar-base" ).css( "background-image", id );
  }
  
  function SetOutfit( id )
  {
    console.log( "SetOutfit(", id, ")" );
    $( "#avatar-outfit" ).css( "background-image", id );
  }
  
  function SetEars( id )
  {
    console.log( "SetEars(", id, ")" );
    $( "#avatar-ears" ).css( "background-image", id );
  }
  
  function SetFace( id )
  {
    console.log( "SetFace(", id, ")" );
    $( "#avatar-face" ).css( "background-image", id );
  }
  
  function SetAccessory( id )
  {
    console.log( "SetAccessory(", id, ")" );
    $( "#avatar-accessory" ).css( "background-image", id );
  }
  
  function SetHair( id )
  {
    console.log( "SetHair(", id, ")" );
    $( "#avatar-hair" ).css( "background-image", id );
  }
  
  function Randomize()
  {
    console.log( "Randomize()" );
    
    SetBase       ( "url( '" + assetPath + GetRandom( bases )   + "')" );
    SetOutfit     ( "url( '" + assetPath + GetRandom( outfits ) + "')" );
    SetFace       ( "url( '" + assetPath + GetRandom( faces )   + "')" );
    //SetEars       ( "url( '" + assetPath + GetRandom( ears )    + "')" );
    //SetAccessory  ( "url( '" + assetPath + GetRandom( accessories )    + "')" );
    SetHair       ( "url( '" + assetPath + "hair-c1-s5.png" + "')" );
  }
  
  function InitializeButtons()
  {
    //for ( var i = 0; i < backgrounds.length; i++ )  { $( "#backgrounds-options" ).append( "<button class='option option-bg' style='background: " + backgrounds[i] + ";'></button>" ); }
    //for ( var i = 0; i < bases.length; i++ )        { $( "#bases-options" ).append( "<button class='option option-base' style='background: url( \"" + assetPath + bases[i] + "\" )'></button>" ); }
    //for ( var i = 0; i < outfits.length; i++ )      { $( "#outfits-options" ).append( "<button class='option option-outfit' style='background: url( \"" + assetPath + outfits[i] + "\" );'></button>" ); }
    //for ( var i = 0; i < ears.length; i++ )         { $( "#ears-options" ).append( "<button class='option option-ear' style='background: url( \"" + assetPath + ears[i] + "\" );'></button>" ); }
    //for ( var i = 0; i < faces.length; i++ )        { $( "#faces-options" ).append( "<button class='option option-face' style='background: url( \"" + assetPath + faces[i] + "\" );'></button>" ); }
    //for ( var i = 0; i < accessories.length; i++ )  { $( "#accessories-options" ).append( "<button class='option option-accessory' style='background: url( \"" + assetPath + accessories[i] + "\" );'></button>" ); }
  }
  
  function GetRandom( fromList )
  {
    return fromList[ Math.floor( Math.random() * fromList.length ) ];
  }
  
  Randomize();
  InitializeButtons();
  OpenPane( "backgrounds" );
} );
